<?php

require_once 'Cube/Filter/Abstract.php';

class MediaFilter extends Cube_Filter_Abstract
{
	public function filter()
	{	
		$filters    = array('title' => 'Clear', 'description' => 'Clear', 'cid' => 'Clear', 'movie' => 'Clear');
		$validators = array('title' => 'Required', 'cid' => 'Required');
		$messages   = array('title' => 'Pole "Tytuł" jest wymagane', 'cid' => 'Pole "Kategoria" jest wymagane'); 	
		$this->_process($filters, $validators, $messages);	
	}
}
					 
?>
