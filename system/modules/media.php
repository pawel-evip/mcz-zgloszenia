<?php

$moduleInfo = array(
			'name' 			=> 'media',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Moduł medialny',
			'info' 			=> 'v1.0, Rafał Liszka, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'files';
$moduleActions[] = 'newfiles';
$moduleActions[] = 'settings';

$moduleTablesLang = array();
$moduleTablesLang = array
	(
		array('table'=>'articles2', 'field'=>array('meta','author','title','contents') ),
		array('table'=>'articles2_test', 'field'=>array('meta','author','title','contents') )
	);


$moduleInstall = array();
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_page", "16", "media")';
$moduleInstall[] = "CREATE TABLE `media_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `pos` int(6) unsigned NOT NULL default '0',
  `name` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `active` tinyint(1) NOT NULL default '1',
  `description` text collate utf8_polish_ci,
  PRIMARY KEY  (`id`),
  KEY `parent_id_pos` (`parent_id`,`pos`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";

$moduleInstall[] = "CREATE TABLE `media_tree_pos` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  `depth` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `parent_id_child_id_depth` (`parent_id`,`child_id`,`depth`),
  KEY `child_id_depth` (`child_id`,`depth`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
					
/*$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `add_date` int(16) NOT NULL default '0',
  `filename` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `title` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `description` text collate utf8_polish_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";

$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `media_photos` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `add_date` int(16) NOT NULL default '0',
  `filename` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `title` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `description` text collate utf8_polish_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";*/

$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `media_docs` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `add_date` int(16) NOT NULL default '0',
  `filename` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `title` varchar(255) collate utf8_polish_ci NOT NULL default '',
  `description` text collate utf8_polish_ci NOT NULL,
  `size` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
					
/* CONFIG */					
$moduleInstall[] = 'INSERT INTO media_categories VALUES (null, 0, 0,"Główna", 1,"")';
$moduleInstall[] = 'INSERT INTO media_tree_pos VALUES (null, 1, 1,0)';
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `media_categories`";
$moduleUninstall[] = "DROP TABLE `media_tree_pos`";
//$moduleUninstall[] = "DROP TABLE `media`";
//$moduleUninstall[] = "DROP TABLE `media_photos`";
$moduleUninstall[] = "DROP TABLE `media_docs`";
$moduleUninstall[] = 'DELETE FROM config WHERE segment = "media"';

?>
