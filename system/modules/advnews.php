<?php

$moduleInfo = array(
			'name' 			=> 'advnews',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Moduł zaawansowanych aktualności',
			'info' 			=> 'v1.0, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'insert';
$moduleActions[] = 'categories';
$moduleActions[] = 'newcomments';
$moduleActions[] = 'comments';
$moduleActions[] = 'settings';


$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews` (
					  `id` int(11) NOT NULL auto_increment,
					  `add_date` int(11) NOT NULL default '0',
					  `author` varchar(255) NOT NULL default '',
					  `title` varchar(255) NOT NULL default '',
					  `meta_title` varchar(255) NOT NULL default '',
					  `filename` varchar(255) NOT NULL default '',
					  `contents` text NOT NULL default '',
					  `short_contents` text NOT NULL default '',
					  `language` varchar(3) NOT NULL default '',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";

$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews_categories` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `amount` int(11) NOT NULL default '0',
					  'max_chars_main` smallint(6) NOT NULL default '300',
					  'max_chars_advnews` smallint(6) NOT NULL default '300',
					  `active` tinyint(1) NOT NULL default '1',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
					
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advnews_category` (
					  `id` int(11) NOT NULL auto_increment,
					  `nid` int(11) NOT NULL default '0',
					  `cid` int(11) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";					
					
					
/* CONFIG */					
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_page", "6", "advnews")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "comments", "1", "advnews")';
$moduleInstall[] = 'INSERT INTO config VALUES (null, "max_chars", "300", "advnews")';
$moduleInstall[] = 'INSERT INTO advnews_categories VALUES (null, "główna", 0, 1)';
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `advnews`";
$moduleUninstall[] = "DROP TABLE `advnews_categories`";
$moduleUninstall[] = "DROP TABLE `advnews_category`";
$moduleUninstall[] = 'DELETE FROM config WHERE segment = "advnews"';

?>
