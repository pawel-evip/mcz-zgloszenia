<?php

$moduleInfo = array(
			'name' 			=> 'menus',
			'admin_access' 	=> 'root',
			'access' 		=> 'none',
			'description' 	=> 'Zarządzaj menu',
			'info' 			=> 'v1.1, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';

$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `menus` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `language` varchar(3) NOT NULL default '',
					  `active` tinyint(1) NOT NULL default '1',
					  `logged` tinyint(1) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
$moduleInstall[] = "CREATE TABLE `menus-rows` (
					  `id` int(11) NOT NULL auto_increment,
					  `mid` int(11) NOT NULL default '0',
					  `label` varchar(255) NOT NULL default '',
					  `href` varchar(255) NOT NULL default '',
					  `active` tinyint(1) NOT NULL default '1',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `menus`";
$moduleUninstall[] = "DROP TABLE `menus-rows`";

?>
