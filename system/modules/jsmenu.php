<?php

$moduleInfo = array(
			'name' 			=> 'jsmenu',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'none',
			'description' 	=> 'Moduł menu rozwijane',
			'info' 			=> 'v1.0, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';


$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `jsmenu` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '', 
					  `extra` varchar(255) NOT NULL default '',
					  `active` tinyint(1) NOT NULL default '1',
					  `href` varchar(255) NOT NULL default '',
					  `pos` int(11) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";

$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `jsmenu_links` (
					  `id` int(11) NOT NULL auto_increment,
					  `mid` int(11) NOT NULL default '0',
					  `name` varchar(255) NOT NULL default '',
					  `href` varchar(255) NOT NULL default '',
					  `extra` varchar(255) NOT NULL default '',
					  `pos` int(11) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
					
/* CONFIG */					
				
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `jsmenu`";
$moduleUninstall[] = "DROP TABLE `jsmenu_links`";

?>
