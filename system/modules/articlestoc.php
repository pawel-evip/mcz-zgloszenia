<?php

$moduleInfo = array(
			'name' 			=> 'articlestoc',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Bloki informacyjne',
			'info' 			=> 'v1.1, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'add';

$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `articles-toc` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `contents` text NOT NULL, 
					  `language` varchar(3) NOT NULL default '',
					  `logged` tinyint(1) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `articles-toc`";

?>
