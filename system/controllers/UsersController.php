<?php 

class UsersController extends Cube_Controller_Abstract
{
	//private $_modules;  //spis wszystkich dostepnych modulow bez modules
	//private $_modules_access; //spis wszystich modułow bez modules dostępnych dla wybranego id.
	//private $_id_admin;	//id admina ktorego chcemy zmienic chaslo.
	
	public function indexAction()
	{		
		$this->_id = $this->_session->getUserId();
		$model = new Users();
		$login=$model->getLogin($this->_id);
		$this->view->login=$login['login'];
		$row = $model->get($this->_id);
		$this->view->name 	  = $row['name'];
		$this->view->surname   = $row['surname'];
		$this->view->email 	  = $row['email'];
		$this->view->phone 	  = $row['phone'];

	
	}
	
	
	public function init()
	{		
		//$this->view->id = $this->_id = $this->_request->getParam('id'); 
		//$model = new Modules();
		//$this->_modules=$this->view->rows = $model->getAllModules('name <> "modules" AND active=1', 'name');
		//if (IsSet ($this->_id))
		//{
		//	$where='m.name <> "modules" AND m.active=1 AND u.id='.$this->_id;
		//	$this->_modules_access = $this->view->modules_access = $model->getModules($where, 'm.name');
		//}
	}

	private function passwd($id)
	{
		$this->view->render('index');
		$model = new Users();
		$id_user = $id;			
		//echo 'Jestem w passwd Id='.$id_user;
		
		if ($this->_request->isRedirected()) {
			//echo 'Jestem w pierwszym IF';
			//exit;
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			//$this->view->id = $this->view->row['id'];	
			return;
		}
		
		if ($this->_request->isPost()) 
		{
			//echo 'Jestem w drugim IFId='.$id_user;
			//return;	
			$filter = new AdminsPasswdFilter();
			$filter->filter();			
			$data = $filter->getData();
			$model = new Users();
			if ($data['pass'] !== $data['repass']) 
				$this->_request->redirectFailure(array('Podane hasła nie są identyczne. Wielkość liter ma znaczenie.'));			
			
			$model->passwd($data,$id_user);	
			header('refresh: 3; url=profil.html');
			$this->view->message = 'Hasło zmienione pomyślnie ! Przekierowywanie...';
		}
	}
		
	public function passwdAction()
	{
		$this->_id = $this->_session->getUserId();			
		$this->passwd($this->_id);
	}	
	
	public function profileAction()
	{
		$this->_id = $this->_session->getUserId();
		$this->view->render('index');
		$model = new Users();
				if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$row= $this->_request->getParamsFromLastRequest(); 
			$this->view->name 	  = $row['name'];
			$this->view->surname   = $row['surname'];
			$this->view->email 	  = $row['email'];
			$this->view->phone 	  = $row['phone'];
			//$this->view->login=$model->getLogin($row['id']);
			//return;
			//header('refresh: 3; url=admin,admins,edit,id_'.$this->_id_admin.'.html');
			//return;
		}
		
		if ($this->_request->isPost()) 
		{		
			$filter = new AdminsEditFilter();
			$filter->filter();			
			$data = $filter->getData();
			$model = new Users();
			
			if (!preg_match('/^[^@ ]+@[^@ ]+\.[^@ \.]+$/',$data['email']))
				$this->_request->redirectFailure(array('Podano nieprawidłowy adres email.'));	
						
			$id_user=$model->update($data,$this->_id);	
			
			header('refresh: 3; url=profil.html');
			$this->view->message = 'Dane zmienione pomyślnie ! Przekierowywanie...';	
		}
	}
	
}	

?>
