<?php 

class Admin_ArticlesController extends Cube_Controller_Abstract
{
	private $_act = null;
	private $_id = null;
	
	private $_id_state=null;
	private $_title_state=null;
	private $_adddate_state=null;
	private $_author_state=null;
	private $_search=null;
	
	private function create_where($search)
	{
		$where_title='id like "%'.$search.'%"';
		$where_id='title like "%'.$search.'%"';
		$where_author='author like "%'.$search.'%"';
		$where='('.$where_title.' OR '.$where_id.' OR '.$where_author.')';
		return $where;
		
	}
	
	private function sort($field,$sort)
	//public function sortLoginAction()
	{
		
		$model = new Articles();
		//$field='Login';
		
		
		if ($sort==1)
			$order='ASC';
		elseif ($sort==2)
			$order='DESC';
	
	
		$where=$this->create_where($this->_search);
		
		$this->view->rows = $model->getAll($where,''.$field.' '.$order.'');
		//sort($order, $field);
		$this->view->render('index');
		
	}
	
	
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->_act = $this->_request->getParam('act');	
		$this->_id = $this->_request->getParam('id');	
		$this->view->id = $this->_id;
		
		$this->view->id_state = $this->_id_state = $this->_request->getParam('idstate');
		$this->view->title_state = $this->_title_state = $this->_request->getParam('titlestate');	
		$this->view->adddate_state = $this->_adddate_state = $this->_request->getParam('adddatestate');
		$this->view->author_state = $this->_author_state = $this->_request->getParam('authorstate');	
		$this->view->search = $this->_search = $this->_request->getParam('search');						   
	}

	public function indexAction()
	{	
		$model = new Articles();
		$this->view->rows = $model->getAll(null, 'id DESC');	
	}

	public function searchAction()
	{
		$search=$_POST['search'];
		//echo 'search ='. $search;
		$model = new Articles();
		$where=$this->create_where($search);
		$this->view->rows = $model->getAll($where,null);
		if (sizeof($this->view->rows) < 1)
		{
			//echo "Jestem w IF";
			header('refresh: 3; url=admin,articles.html');
			$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
			$this->view->render('index');
	
			return;
		}
		$this->view->render('index');
	}
		
	public function sortAuthorAction()
	{
		$field='author';
		$sort=$this->_author_state;
		$this->sort($field,$sort);		
	}
	
	public function sortAddDateAction()
	{
		$field='add_date';
		$sort=$this->_adddate_state;
		$this->sort($field,$sort);		
	}
		
	
	public function sortTitleAction()
	{
		$field='title';
		$sort=$this->_title_state;
		$this->sort($field,$sort);		
	}
	
	public function sortIDAction()
	{
		$field='id';
		$sort=$this->_id_state;
		$this->sort($field,$sort);		
	}


	/*public function tocAction()
	{
		$model = new Articles();
		$this->view->rows = $model->getAllTOC(null, 'language, id DESC');	
	}

	public function addtocAction()
	{		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new ArticlesTOCFilter();
			$filter->filter();			
			$model = new Articles();
			$model->insertTOC($filter->getData());	
			header('refresh: 3; url=admin,articles,toc.html');
			$this->view->message = 'Tabela zawartości dodana pomyślnie ! Przekierowywanie...';
		}
	}

	public function toceditAction()
	{
		$model = new Articles();
		$row = $model->getTOC($this->_id);
		$this->view->row = $row;
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new ArticlesTOCFilter();
			$filter->filter();
			$model->updateTOC($this->_id, $filter->getData());
			header('refresh: 3; url=admin,articles,toc.html');
			$this->view->message = 'Tabela zawartości zaktualizowana pomyślnie ! Przekierowywanie...';	
		}
	}

	public function tocdeleteAction()
	{
		$model = new Articles();
		$model->deleteTOC($this->_id);
		header('refresh: 3; url=admin,articles,toc.html');
		$this->view->message = 'Tabela zawartości usunięta pomyślnie ! Przekierowywanie...';
		$this->view->render('tocedit');
	}*/	

	public function insertAction()
	{		
		$this->view->row['author'] = $this->_session->getUsername();
				
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new ArticlesFilter();
			$filter->filter();			
			$model = new Articles();
			$model->insert($filter->getData());	
			header('refresh: 3; url=admin,articles.html');
			$this->view->message = 'Podstrona/artykuł dodana pomyślnie ! Przekierowywanie...';
		}
	}
	
	public function editAction()
	{
		$model = new Articles();
		$row = $model->get($this->_id);
		$this->view->row = $row;
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new ArticlesFilter();
			$filter->filter();
			$model->update($this->_id, $filter->getData());
			header('refresh: 3; url=admin,articles.html');
			$this->view->message = 'Podstrona zaktualizowana pomyślnie ! Przekierowywanie...';	
		}
	}
	
	public function deleteAction()
	{
		$model = new Articles();
		$model->delete($this->_id);
		header('refresh: 3; url=admin,articles.html');
		$this->view->message = 'Podstrona usunięta pomyślnie ! Przekierowywanie...';
		$this->view->render('edit');
	}		
}

?>
