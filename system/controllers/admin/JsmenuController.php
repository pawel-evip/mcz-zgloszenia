<?php 

class Admin_JsmenuController extends Cube_Controller_Abstract
{
	private $_id = null;
	
	public function init()
	{
		$this->view->setTemplate('admin');		
		$this->_id = $this->view->id = $this->_request->getParam('id', 0);	
		
		$this->view->ArticlesList = $this->_getArticlesList();				   
	}
	
	public function redirectAction()
	{
		header('Location: admin,jsmenu.html');
	}	
	
	public function indexAction()
	{			
		$model = new Jsmenu();
		$menus = (array)$model->getMenus();
		
		foreach ($menus as $k => $menu)
		{
			$links = $model->getLinks($menu['id']);
			$menus[$k]['links'] = $links;
		}
		
		$this->view->rows = $menus;
	}

    public function _getArticlesList()
    {
		$temp = '';
		$model = new Articles();
		$rows = $model->getAll(null, 'id DESC');
		foreach($rows as $r)
		{
				$link=$r['title'].'-'.$r['id'].'.html';
				$link=str_replace(' ','_',$link);
				$temp .= '<option value="'.$link.'">'.$link.'</option>';
		}
		//header('refresh: 3; url=admin,jsmenu.html');
		//$this->view->message = 'TesT:'.$temp;
		return $temp;
	}	
	
	public function editAction()
	{
		$model = new Jsmenu();
		$this->view->row = $row = $model->getMenu($this->_id);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new JsmenuFilter();
			$filter->filter();
			$model->updateMenu($this->_id, $filter->getData());
			header('refresh: 3; url=admin,jsmenu.html');
			$this->view->message = 'Menu zaktualizowane  pomyślnie ! Przekierowywanie...';	
		}
	}

	public function menuinsertAction()
	{
		$this->view->render('index');
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = '';			
			return;
		}	
		
		if ($this->_request->isPost()) {
			$filter = new JsmenuFilter();
			$filter->filter();			
			$model = new Jsmenu();			
			$model->insertMenu($filter->getData());	
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = 'Menu dodane pomyślnie ! Przekierowywanie...';	
		}
	}

	public function linkinsertAction()
	{
		$params = $this->_request->getParamsFromLastRequest(); 
		$this->view->address 	  = $params['href'];
				
		$this->view->render('index');
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = '';			
			return;
		}	
		
		if ($this->_request->isPost()) {
			$filter = new JslinkFilter();
			$filter->filter();			
			$model = new Jsmenu();			
			$model->insertLink($filter->getData());	
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = 'Link dodane pomyślnie ! Przekierowywanie...';	
		}
	}

	public function updatemenuAction()
	{
		$this->view->render('index');
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = '';			
			return;
		}	
		
		if ($this->_request->isPost()) {
			$filter = new JsmenuFilter();
			$filter->filter();			
			$model = new Jsmenu();			
			$model->updateMenu($this->_id,$filter->getData());	
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = 'Menu zaktualizowane pomyślnie ! Przekierowywanie...';	
		}
	}
	
	public function updatelinkAction()
	{
		$this->view->render('index');
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = '';			
			return;
		}	
		
		if ($this->_request->isPost()) {
			$filter = new JslinkFilter();
			$filter->filter();			
			$model = new Jsmenu();			
			$model->updateLink($this->_id,$filter->getData());	
			header('refresh: 3; url=admin,jsmenu,index.html');
			$this->view->message = 'Link zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}

	public function deletemenuAction()
	{
		$this->view->render('index');	
		$model = new Jsmenu();			
		$model->deleteMenu($this->_id);	
		header('refresh: 3; url=admin,jsmenu,index.html');
		$this->view->message = 'Menu usunięte pomyślnie ! Przekierowywanie...';	
	}

	public function deletelinkAction()
	{
		$this->view->render('index');	
		$model = new Jsmenu();			
		$model->deleteLink($this->_id);	
		header('refresh: 3; url=admin,jsmenu,index.html');
		$this->view->message = 'Link usunięty pomyślnie ! Przekierowywanie...';	
	}

	public function activeAction()
	{
		$this->view->render('index');	
		$model = new Jsmenu();			
		$model->activeMenu($this->_id);	
		header('refresh: 3; url=admin,jsmenu,index.html');
		$this->view->message = 'Menu aktywowane pomyślnie ! Przekierowywanie...';	
	}

	public function deactiveAction()
	{
		$this->view->render('index');	
		$model = new Jsmenu();			
		$model->deactiveMenu($this->_id);	
		header('refresh: 3; url=admin,jsmenu,index.html');
		$this->view->message = 'Menu deaktywowane pomyślnie ! Przekierowywanie...';	
	}
}

?>
