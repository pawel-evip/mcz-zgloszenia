<?php 

class AdvgalleryController extends Cube_Controller_Abstract
{	
	private $_pp; // photos per page

	public function init()
	{
		$this->_categoriesModel = new AdvGalleryCategories();
		$this->_pp = $this->_config->get('per_page', 'advgallery');
	}

	# zwraca tablice postaci: id => sciezka typu Top/Test/Subtest, gdzie ID to nr id kategorii subtest
	private function _getTree($parent_id, $current_name, &$tree)
	{	
		$subcats = $this->_categoriesModel->getSubcategories($parent_id);
		if (count($subcats) < 1)
			return $tree;
			
		foreach ($subcats as $sub)
		{
			$tree[$sub['id']] = $current_name.$sub['name'].'/';
			$this->_getTree($sub['id'], $tree[$sub['id']], $tree);	
		}	
		
		return $tree;
	}

	public function _getCategories()
	{
		$tree = array(1 => 'Galeria/');
		$tree = (array)$this->_getTree(1, 'Galeria/', $tree);
		asort($tree);
		
		//print_r($tree);
		return $tree;	
	}

	public function _getCategoriesList($cid = null, $no = false)
	{
		$tree = array(1 => 'Galeria/');
		$tree = (array)$this->_getTree(1, 'Galeria/', $tree);
		asort($tree);
		
		$temp = '';
		foreach ($tree as $id => $name)
		{
			if (!is_null($cid) && $id == $cid) {
				if (!$no) $temp .= '<option value="'.$id.'" selected="selected">'.$name.'</option>';
			} else $temp .= '<option value="'.$id.'">'.$name.'</option>';
		}
		return $temp;	
	}

	protected function _getSubCategories($parent_id)
	{
		$cats = $this->_categoriesModel->getSubcategories($parent_id);
		return $cats;
	}
	
	public function indexAction()
	{		
		$cid = clear($_POST['category']);
		if ($cid == 0 || $cid == '' || !is_numeric($cid)) $cid = $this->_request->getParam('cid', 1);
		$this->view->cid = $cid;
		
		$subcats = $this->_getSubCategories($cid);
		$cats = new AdvGalleryCategories();
		
		$current_cat_path = $this->_getCategories();
		$this->view->currentCatPath = $current_cat_path = $current_cat_path[$this->view->cid];	
		
		if (count($subcats) > 0) {
			$this->view->subcatsExists = true;
		
			foreach ($subcats as $k => $v)
			{
				$latest = $cats->getLastPhoto($v['id']);
				$subcats[$k]['filename'] = $latest['filename'];	
				$subcats[$k]['path'] = $v['path'];
				if ($latest['filename'] == '')
				{
					$sss = $this->_getSubCategories($v['id']);
					foreach ($sss as $kkk => $vvv)
					{
						$latest = $cats->getLastPhoto($vvv['id']);
						$subcats[$k]['filename'] = $latest['filename'];	
						$subcats[$k]['path'] = $vvv['path'];
						if ($latest['filename'] == '')
						{
							$ssss = $this->_getSubCategories($vvv['id']);
							foreach ($ssss as $kkkk => $vvvv)
							{
								$latest = $cats->getLastPhoto($vvvv['id']);
								$subcats[$k]['filename'] = $latest['filename'];	
								$subcats[$k]['path'] = $vvvv['path'];
							}
						} else continue;
					}
				} else continue;	
			}
			
			$this->view->subcats = $subcats;	
		} else {
			$this->view->subcatsExists = false;
			$model = new AdvGallery();
			$photos = $model->getAll('p.cid = "'.$cid.'"', 'p.id DESC');
			$photos_amount = sizeof($photos);
			$template = ' <a href="galeria_'.$cid.',:value.html">[:id]</a> ';
			$start = $this->_request()->getParam('start', 0);
			
			$this->view->currentCat = $cats->get($cid);
			
			// PAGES
			Cube_Loader::loadClass('Cube_Pages_Advanced');
			$pages = new Cube_Pages_Advanced($start, $photos_amount, $this->_pp, $template);
				
			$tmp = array();
			foreach ($photos as $k => $v)
			{
				if ( ($k + 1) > $start && ($k + 1) <= ($start + $this->_pp) ) 
					$tmp[] = $v;
			}
				
			$this->view->pages = $pages;
			$this->view->rows = $tmp;
		}
	}		
}

?>
