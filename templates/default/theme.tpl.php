<?php 
	/*
		THEME.TPL.PHP to główny plik szablonu, tzn. w tym pliku definiuje sie strukturę strony.
		W tym pliku (i wszystkich plikach kontrolerów, czyli np: news_index.tpl.php) są dostępne następujące zmienne:
		
			1. $controller = zmiena zwracająca całą zawartość akcji, czyli zawartośc wygenerowana 
							 np przez news_index.tpl.php, czyli jakby przez "podstronę", moduł.
							 THEME.TPL.PHP musi być tak napisany, aby w połączeniu z wygenerowaną zawartością podstron
							 (czyli pozostalych plikow z katalogu teplates/default/) tworzyły kompletną strone.
							 Mam nadzieje ze mnie rozumiesz, chodzi o to że plik każda akcja (np: wyswietlenie newsa, wszystkich newsow, podstrony)
							 jest generowana przez inny plik, np: news_index.tpl.php, news_view.tpl.php i wygenerowana 
							 zawartosc jest wrzycana w miejsce $controller t theme.tpl.php
							 UWAGA:  $controller uzywaj tylko w THEME.TPL.PHP
			
			2. $meta = wyświetla znaczniki meta keywords, meta description i title wygenerowane na podst. danych 
					   ustawionych w panelu i zalezne od języka.
					   UWAGA:  $meta uzywaj tylko w THEME.TPL.PHP	
			
			3. $footer = wyświetla zawartość stopki ustaloną w panelu adm.
			
			4. $moduleName = nazwa obecnie uruchomionego modułu
			5. $language = obecny język w postaci krótkiej, np: 'pl', 'en'
			6. $styles = jeśli w katalogu 'templates/default/styles/' znajduje się plik style_JEZYK.css  
						 np style_pl.css i style_en.css to zmienna $styles zawiera znacznik <link rel...> 
						 tego własnie stylu. Używane przy korzystaniu z wielu języków, np w style.css są domyślne 
						 obrazki tła, a w style_pl.css i style_en.css są nadpisane style, zawierające odpowieniki 
						 obrazków dla danego języka, wtedy własnie ten styl zostanie załadowany.
						 
			7. $languagesDir = zwraca 'templates/default/languages/OBECNY_JEZYK/'
			8. $stylesDir = zwraca 'templates/default/styles/'
			9. $imagesDir = zwraca 'templates/default/images/'
			10.	$currentURL = zawiera kawałek ścieżki odpowiadający za obecne żadanie. 
							  za pomocą $currentURL mozemy sprawdzić jaka akcja wykonywana jest obecnie,
							  wykorzystuje się przy generowaniu menu.
							  
			11. Zmienne typu $menu_NRMENU gdzie NRMENU to ID odpowiadający menu z panelu administracyjnego, 
				przykład wykorzystania poniżej.	
				Wszystkie zmienne $menu_X są dwuwymiarowymi tablicami, które po potraktowaniu foreach'em posiadaja:
				$menu_X['label'] = wyświetlany tytuł odnośnika
				$menu_X['href'] = adres odnośnika			  			   				 
				 
	*/
	
	//$latestNewsBlock = $this->getBlock('latestNews');	
	//$latestNews = (array)$latestNewsBlock->getLatest();	// $latestNews = dwuwymiarowa tablica zawierająca ostatnie X newsów (X do ustawienia w panelu)
													// jak zwykle - trzeba potraktować ją foreach'em
	//$jsMenu = $this->getBlock('JsMenu');	
														
?>

<?php 
/*
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php echo str_replace('&oacute;', 'ó', $meta); ?>
<script type="text/javascript" src="templates/default/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="templates/default/js/jquery.lightbox.min.js"></script>
</head>
<body>
<h3>eVip CMS v1.2.1 (25.10.2008)</h3>
<p>Brak szablonu. Musisz wgrać szablon!<br /></p>
<p><a href="admin.html">Panel administracyjny</a></p>
</body>
</html>*/
?>

<?php

if (!($this->_session->isLoggedIn()))
{

echo '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Nazwa aplikacji - System Obsługi Zgłoszeń: system autoryzacji</title>
	<link rel="stylesheet" type="text/css" href="templates/default/styles/style.css" />
</head>
<body>

<div id="wrap">
	
	<a href="index.html" id="logo" title="MCZ"></a>
	
	<div id="controler">
	
		<div id="zaloguj">Zaloguj się</div>
		
		<div id="zaloguj2">
			<form action="auth,login.html" method="post">
				<table cellpadding="0" cellspacing="0">
				<tr><td>login</td><td><input type="text" name="username" value="" /></td></tr>
				<tr><td>hasło</td><td><input type="password" name="password" value="" /></td></tr>
				<tr><td></td><td><input type="submit" value="Wyślij" class="submit" /><input type="reset" value="Wyczyść" class="submit" /></td></tr>
				</table>
			</form>
		</div>
	
	</div>
	<div id="info">Jeśli nie masz dostępu skontaktuj się z administratorem systemu <a href="mailto:mcz@mcz.pl">mcz@mcz.pl<a/></div>

</div>
</body>
</html>';

}

else
{



echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="templates/default/styles/main.css" rel="stylesheet" type="text/css" />
<script src="templates/default/js/fla.js" type="text/javascript"></script>';
echo str_replace('&oacute;', 'ó', $meta); 
echo '<script type="text/javascript" src="templates/default/scripts/jquery.min.js"></script>
<script type="text/javascript" src="templates/default/scripts/all_scripts.js"></script>
<script type="text/javascript" src="templates/default/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="templates/default/js/lightbox/jquery.lightbox.js"></script>
<script type="text/javascript" src="templates/default/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="public/css/jquery.Jcrop.css" type="text/css" />
<script src="templates/default/js/flowplayer-3.2.4.min.js"></script>
<script src="templates/default/js/script.js"></script>
<link rel="stylesheet" type="text/css" href="templates/default/styles/style.css" />

<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/general.js"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			elements : "ajaxfilemanager",
			theme : "advanced",
			editor_selector : "tiny",
			plugins :"safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			

			
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,forecolor,backcolor,|,cut,copy,paste",
			theme_advanced_buttons2 : "search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "ajaxfilemanager",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : false,
			force_p_newlines : false,	
			relative_urls : true
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "entry":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "800";
			fileBrowserWindow["height"] = "600";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
	</script>

</head>

<body><div id="wrap"><a href="index.html" id="logo" title="MCZ"></a><div id="controler">';

	echo '<div id="logout">zalogowany jako: <span>'.$this->_session->getUsername().'</span>';
	if (strcmp($this->_session->getUsername(),'pliki'))
		echo '<br /><a href="profil.html">Profil</a> | <a href="auth,logout.html">Wyloguj</a></div>';
	else
		echo '<br /> <a href="auth,logout.html">Wyloguj</a></div>';

	$rozwijane = $this->getBlock('RozwijaneMenu');
	//Tomek. Menu dla usera ktory ma uprawniena.
	$id_user=$this->_session->getUserId();
	$rozwijane = $rozwijane->get($id_user);	
	$menu2 = $rozwijane[0]['links'];
	
	//print_r($rozwijane);
	//exit;
			
	echo '<div id="menu">';

	function checkActivity($currentURL, $href, $r, $row)
	{
		/*if ((strpos($currentURL, 'aktualnosci') !== false && strpos($href, 'aktualnosci') !== false) ||
				 	((strpos($currentURL, 'wydarzenia.html') !== false || strpos($currentURL, 'wydarzenia,') !== false) && strpos($href, 'wydarzenia.html') !== false)  ||
				 	(strpos($currentURL, 'wydarzenia_ostatnio') !== false && strpos($href, 'wydarzenia_ostatnio') !== false) ||
				 	(strpos($currentURL, 'sklepy') !== false && strpos($href, 'sklepy') !== false) ||
				 	(strpos($currentURL, 'promocje,') !== false && strpos($href, 'promocje.') !== false) ||
				 	(strpos($currentURL, 'dodaj_promocje') !== false && strpos($href, 'dodaj_promocje') !== false) ||
				 	(strpos($currentURL, 'galeria') !== false && strpos($href, 'galeria') !== false) ||
				 	(strpos($currentURL, 'media_51') !== false && strpos($href, 'media_51') !== false) ||
				 	(strpos($currentURL, 'media.') !== false && strpos($href, 'media.') !== false))
			*/
					 return true;
		
		// przypadek dla eventów
		//if ($r->getController() == 'events' && $r->getAction() == 'view') {
		//	if ($row['event']['past'] == 2 && $row['event']['active'] == 1) { // ostatnio
		//		if (strpos($href, 'wydarzenia_ostatnio') !== false) 
		//			return true;
		//	} else {
		//		if (strpos($href, 'wydarzenia.') !== false) 
		//			return true;
		//	}
		//}
					 
		//return false;			 	
	}
			
	$main = null;
	foreach ($rozwijane as $menu) 
	{
			$show = null;
			if ($currentURL == $menu['href'] || checkActivity($currentURL, $menu['href'], $this->_request, $row))
			{
				$menu2 = $menu['links'];
				$show = '<a href="'.$menu['href'].'" title="'.$menu['extra'].'" class="mark">'.$menu['name'].'</a>';
			} else {	
				$show = '<a href="'.$menu['href'].'" title="'.$menu['extra'].'">'.$menu['name'].'</a>';	
				foreach($menu['links'] as $l) // sprawdz, moze jednak jakis z podlinkow inncyh niz wyzej
				{
					if ($currentURL == $l['href'] || checkActivity($currentURL, $l['href'], $this->_request, $row))
					{
						$menu2 = $menu['links'];
						$show = '<a href="'.$menu['href'].'" title="'.$menu['extra'].'" class="mark">'.$menu['name'].'</a>';
					}
				}			
			}
			echo $show;	
	}
	
	echo '</div>';
	
	// echo $controller;
	
	
	
	
	
	if($this->_request->getController() == 'media' || $this->_request->getController() == 'entry')
	{

		if ($this->_request->getController() == 'media') {
			$name = 'pliki';
		} else {
            $name = 'rozporzadzenia';
		}
		
		echo '<div class="leftside">';
		//echo 'print='.$this->cid;	
		//print_r($this->left_menu);
		if ( count($this->left_menu) > 0 )
		{
			//$tab='&nbsp;&nbsp;&nbsp;';
			foreach($this->left_menu as $info)
			{
				$id=$info['id'];
				$depth=null;
				if($info['name'] != 'Korzeń')
				{
					$depth=$info['depth']+1;
					//echo 'depth='.$info['depth'];
					//for ($i=0;$i<$info['depth'];$i++)
					//{
					//	$depth.=$tab;
					//}
					//echo '<br /><strong>'.$depth.$id.'-'.$info['name'].'</strong><br /><br />';
					if ($this->cid==$info['id'])
						//zmien kolor
						echo '<a href="'.$name.'_'.$info['id'].'.html" class="mark_'.$depth.' markme">'.$info['name'].'</a>';
					else	
						echo '<a href="'.$name.'_'.$info['id'].'.html" class="mark_'.$depth.'">'.$info['name'].'</a>';
			
				}	
			}			
		}			
					
					
					/*if ($this->subcatsExists) {
					$subcats = $this->subcats;
					
					foreach ($subcats as $s)
						{
						echo '<a href="pliki_'.$s['id'].'.html">'.$s['name'].'</a>';
						}
					} */
				
					
		echo '</div>
			<div class="rightside">'.$controller.'</div>';
		
	}else
	{
		echo ''.$controller.'';
	}
	
	
			
echo '</div></div>
</body>
</html>';

}
?>
