<?php 
	$categoryPath = $this->currentCatPath;
	if ($this->subcatsExists) {
		$subcats = $this->subcats;
		echo '<table>
		<tr>
		<td>
			<h1>Galeria</h1>
		</td>
		</tr>
		</table>
		<table>
		<tr><td colspan="2">'.$categoryPath.'</td></tr>';
		
		foreach ($subcats as $s)
		{
			if ($s['filename'] == '')
				$img = 'public/default.jpg';
			else 
				$img = 'public/advgallery/'.$s['path'].'thumbs/'.$s['filename'];	
		
			echo '<tr>
				<td width="14%">
				<a href="galeria_'.$s['id'].'.html"><img src="'.$img.'" border="0" /></a>
				</td>
				<td width="86%" align="left" valign="middle"><a href="galeria_'.$s['id'].'.html">'.$s['name'].'</a></td>
			</tr>';
		}
		echo '</table>';
	} else {
		$gallery = $this->rows;	
		$pages = $this->pages;
		$ccat = $this->currentCat;
	
		echo '<table><tr><td colspan="3">
		<h1>Galeria</h1></td></tr>
		<tr><td colspan="3">'.$categoryPath.'</td></tr>';
		
		$a = 0;
		foreach ($gallery as $p)
		{
			$a++;
			if ($a == 0) 
				echo '<tr>'; 
				
			$img = 'public/advgallery/'.$ccat['path'].'thumbs/'.$p['filename'];
			$href = 'public/advgallery/'.$ccat['path'].$p['filename'];
			
			echo '<td align="center">
			<a href="'.$href.'" rel="lightbox"><img src="'.$img.'" border="0" /></a>
			</td>';
			
			if ($a == 5) {
				echo '</tr>';
				$a = 0;
			}
		}
		
		if ($a != 0)
			echo '</tr>';
		
		echo '</table>';
		
			// stronnicowanie juz znamy z news_index.tpl.php....	
		if ($pages->hasPrev() || $pages->hasNext()) { 
			echo '<div class="pages-nav">';
			
			if ($pages->hasPrev()) {
				echo $pages->getPrevLink('<a href="galeria_'.$this->cid.',:value.html" class="rr">Poprzednia</a>');
			} else {
				echo '<a href="#" class="rr">Poprzednia</a>';
			}
			
			echo '<p>Strony: '.$pages->getPages() .'</p>';
			
			if ($pages->hasNext()) {
				echo $pages->getNextLink('<a href="galeria_'.$this->cid.',:value.html" class="rr">Następna</a>');
			} else {
				echo '<a href="#" class="rr">Następna</a>';
			}
			
			echo '</div>';
		}
		
	}

?>
