<?php
	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		$logged = null;
		if ($this->row['logged'] == '1') $logged = ' checked="checked"';
	
		echo '<form action="admin,articlestoc,add.html" method="post">
		<fieldset>
			<legend>Dodaj tabelę zawartości</legend>
			<div><label for="language"><span class="b">Język:</span></label><select name="language">'.getSelectLanguages('pl').'</select></div>
			<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" value="'.$this->row['name'].'" /></div>
			<div><label for="logged"><span>Tylko zalogowani:</span></label><input type="checkbox" class="check" name="logged" value="1"'.$logged.' /></div>		
			<div><label for="contents"><span class="b">Treść:</span></label></div>
			<div> <textarea name="contents" class="tiny" id="contents" rows="14">'.$this->row['contents'].'</textarea></div>
			<div><p>* Pola <strong>pogrubione</strong> są wymagane.</p></div>
			<div>
				 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
				 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</div>	
		</fieldset>
		</form>';
	}
?>
