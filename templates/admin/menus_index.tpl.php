<?php 

		// DODAJ NOWA GRUPE MENU
		 echo '<form action="admin,menus,addmenu.html" method="post">
				<fieldset>
					<legend>Dodaj nową grupę (kolejny blok menu): </legend>
					<div><label for="name"><span>Nazwa:</span></label><input type="text" class="short" name="name" value="" /></div>
					<div><label for="language"><span>Język:</span></label><select class="short" name="language">'.getSelectLanguages('pl').'</select>	</div>
					<div><label for="logged"><span>Tylko zalogowani:</span></label><input type="checkbox" class="check short" name="logged" value="1" /></div>	
					<div>
					 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
		  </form>';
		  
		 // WYSWIETL TABELE Z MENUsami 
		 $menus = (array)$this->menus;
		 $rows = (array)$this->rows;
		  
		echo '<h1>Zarządzaj obecnymi menu:</h1>
		<p class="center">UWAGA: w przypadku usunięcia grupy - wszystkie menu należące do grupy zostaną bezpowrotnie usunięte !</p>
		<table id="administrate" class="clear">
					<thead>
						<tr>
							<td>#</td>
							<td>Pozycja</td>
							<td>Odnośnik</td>
							<td>Numer artykuły/adres</td>
							<td>Akcja</td> 
						</tr>
					</thead>
					<tbody>';	
		
		 foreach ($menus as $menu)
		 {
		 	$class = getTableClass();
		 	// WYSWIETLA NAGLOWEK GRUPY - MENU
		 	if ($menu['language'] == 'all') $menu['language_img'] = 'wszystkie';
			else $menu['language_img'] = '<img src="languages/flags/'.$menu['language'].'.gif">';
			
			if ($menu['active']) {
				$status = '<strong class="green">aktywny</strong>'; 
				$act = '<a href="admin,menus,deactive,mid_'.$menu['id'].'.html">deaktywuj</a>';
			}	
			else {
				$status = '<strong class="red">nieaktywny</strong>'; 
				$act = '<a href="admin,menus,active,mid_'.$menu['id'].'.html">aktywuj</a>';
			}
			
			echo '<tr class="menu-header">
					<form action="admin,menus,update,mid_'.$menu['id'].'.html" class="in-table" method="post">
						<td>'.$menu['id'].'<br /><a href="admin,menus,delete,mid_'.$menu['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">usuń</a></td>
						<td>'.$menu['language_img'].'</td>
						<td><span class="b">$menu_'.$menu['id'].'</span> - <span class="b">Status: '.$status.'</span></td>
						<td><input type="text" class="in-table-short" name="name" value="'.$menu['name'].'" /></td>
						<td><input type="submit" value="aktualizuj" class="in-table-short-submit" /> &nbsp; &nbsp;  <span>'.$act.'</span></td> 
					</form>
				</tr>';	
			
			// WYSWIETLA WPISY NALEZĄCE DO MENU
			$latest = 0;
			foreach ($rows as $row)
			{
				if ($row['mid'] == $menu['id']) {		
					echo '<tr'.$class.'>
							<form action="admin,menus,update,id_'.$row['id'].'.html" class="in-table" method="post">
								<td></td>
								<td><input type="text" class="in-table-short" name="pos" value="'.$row['pos'].'" /></td>
								<td><input type="text" class="in-table-short" name="label" value="'.$row['label'].'" /></td>
								<td><input type="text" class="in-table-short" name="href" value="'.$row['href'].'" /></td>
								<td><input type="submit" value="aktualizuj" class="in-table-short-submit" />  <span>|-- <a href="admin,menus,delete,id_'.$row['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">usuń</a></span></td> 
							</form>
						</tr>';
					$latest = $row['pos'];	
				}
			}	
			echo '<tr'.$class.'>
					<form action="admin,menus,addrow.html" class="in-table" method="post">
						<input type="hidden" name="mid" value="'.$menu['id'].'" class="hidden" />
						<td>dodaj:</td>
						<td><input type="text" class="in-table-short" name="pos" value="'.($latest+1).'" /></td>
						<td><input type="text" class="in-table-short" name="label" value="" /></td>
						<td><input type="text" class="in-table-short" name="href" value="" /></td>
						<td><input type="submit" value="dodaj odnośnik" class="in-table-short-submit" /></td> 
					</form>
				</tr>';		
		}
		echo '</tbody></table>';				  	  

?>
