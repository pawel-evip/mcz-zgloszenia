<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
	
	$path=null;
		foreach ($this->currentPath as $row)
		{
			$path.='<a href="admin,media,index,cid_'.$row['id'].'.html" style="color:#36c;">'.$row['name'].'</a>';
			$path.=' &raquo; ';
		}
		$path=substr($path,0,-2);

echo '<h1>Zarządzaj kategoriami</h1>';
	
echo '

<style>
.close {border:1px solid #ccc; padding:5px; background:#f5f5f5; float:left; margin:0 0 10px 5px;}
</style>

<div style="border:1px solid #ccc; padding:10px; background:#fff; margin:0 0 10px 0;">

<div style="border:1px solid #ccc; padding:5px; background:#f5f5f5; margin:0 0 20px 0;">Aktualna kategoria: '.$path.'</div>
<a href="admin,media,index.html" style="border:1px solid #ccc; float:left; padding:5px; background:#f5f5f5;">&laquo; Zobacz wszystkie kategorie &raquo;</a>'.$this->jsTree.'
</div>
';
		

		
		
		
		
			echo '<h2>Zarządzaj aktualnymi podkategoriami</h2>
			<form action="admin,media,poscategories,cid_'.$this->cid.'.html" method="post">	
			<table cellspacing="0" style="width:100%;">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Nazwa</td>
							<td>Ilość dokumentów</td>
							<td class="tocenter">Pozycja</td>
							<td class="tocenter">Status</td>	
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			
			if (count($rows) < 1) {
				echo '<tr><td colspan="6">Nie odnaleziono żadnych kategorii.</td></tr>';
			} 
			else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					
					if ($r['active']) {
						$status = '<span class="green">aktywna</span>';
						$status_action = ' | <a href="admin,media,deactivecat,id_'.$r['id'].',cid_'.$this->cid.'.html" onclick="return confirm(\'UWAGA. Wszystkie podkategorie zostaną również wyłączone\')">Deaktywuj</a>';
					} else {
						$status = '<span class="red">nieaktywna</span>';
						$status_action = ' | <a href="admin,media,activecat,id_'.$r['id'].',cid_'.$this->cid.'.html" onclick="return confirm(\'UWAGA. Wszystkie podkategorie zostaną również włączone\')">Aktywuj</a>';				
					} 				
					if ($r['id']==1) 
					{
						//jezeli korzeń to usun opcje: usun i deaktywuj i edytuj.	
						echo '			<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'</td>
							<td>'.$r['name'].'</td>
							<td>'.$r['doc_amount'].'</td>
							<td class="tocenter">'.null.'</td>
							<td class="tocenter">'.$status.'</td>
							<td class="toright"></td>
						</tr>';
					}
					else
					{				
						echo '			<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'<br /><a href="admin,media,deletecat,id_'.$r['id'].',cid_'.$this->cid.'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
							<td>'.$r['name'].'</td>
							<td>'.$r['doc_amount'].'</td>
							<td class="tocenter"><input type="text" class="in-table-short" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
							<td class="tocenter">'.$status.'</td>
							<td class="toright"><a href="admin,media,editcat,id_'.$r['id'].',cid_'.$this->cid.'.html">Edytuj</a> '.$status_action.'</td>
						</tr>';
					}
				}		
			}
		
			echo '</tbody></table>
			<input type="submit" value="AKTUALIZUJ" class="submit" /><br/>
			
			<a href="admin,media,files,cid_'.$this->cid.'.html" class="add">DODAJ PLIK W WYBRANEJ KATEGORII</a>
			<a href="admin,media,addcat,cid_'.$this->cid.'.html" class="add">DODAJ KATEGORIE W WYBRANEJ KATEGORII</a></form><br /><br /><br /><br />';
			echo '
		<h2>Zarządzaj danymi kategorii</h2>
		<table cellspacing="0" style="width:100%;">
					<thead>
						<tr>
							<td class="tocenter">ID</td>
							<td>Tytuł</td>
							<td class="tocenter">Data dodania</td>
							<td class="tocenter">Kategoria</td>
							<td class="tocenter">Plik</td>
							<td class="tocenter">Rozmiar</td>			
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->docs;
			if (count($rows) < 1) {
				echo '<tr><td colspan="7">Nie odnaleziono żadnych dokumentów w wybranej kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					$size=round($r['size']/1024);
					//$size=round($r['size']);
					$size=number_format($size, 0, ',', ' ');
					$size.=' kB'; 
					echo '<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'<br /><a href="admin,media,deletedoc,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
							<td>'.$r['title'].'</td>
							<td class="tocenter">'.date('Y-d-m',$r['add_date']).'</td>
							<td class="tocenter">'.$r['cat'].'</td>
							<td class="tocenter"><a href="public/media/docs/'.$r['filename'].'" target="_blank">Podgląd</a></td>
							<td class="tocenter">'.$size.'</td>
							<td class="toright"><a href="admin,media,editdoc,id_'.$r['id'].'.html">Edytuj</a></td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>
			
';
	}

?>
