<?php

	$rows = (array)$this->rows;

	echo '<h1>Zarządzaj administratorami</h1>
		<table id="tab-admins">
			<thead>
				<tr>
					<td>ID</td>
					<td>Login</td>
					<td>Email</td>
					<td>Rola</td>
					<td>Status konta</td>
					<td>Mail</td>
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';

		foreach ($rows as $row) 
		{
			if ($this->_session->getRole() == 'administrator') 
			{
				if ($row['role'] == 'root')
					continue;
				
			}
			if ($row['active']) {
				$status = '<span class="green">aktywne</span>';
				$act = 'deaktywuj';
				$sact = 'deactive';
			}	
			else {
				$status = '<span class="red">nieaktywne</span>';
				$act = 'aktywuj';
				$sact = 'active';
			}

			if ($row['mail'] == 1) {
				$mail='<br /><a href="admin,admins,deactivemail,id_'.$row['id'].'.html" >TAK</a>';
			}	
			else {
				$mail='<br /><a href="admin,admins,activemail,id_'.$row['id'].'.html" >NIE</a>';
			}	
			
			$role = '<form class="access" action="admin,admins,role,id_'.$row['id'].'.html" method="post">
				<fieldset>
				<select name="role">';
			switch ($row['role'])
			{
				case 'root': $role .= '<option value="root" selected="selected">root</option>
										<option value="administrator">administrator</option>
										'; 
							 break;
				case 'administrator': $role .= '<option value="root">root</option>
										<option value="administrator" selected="selected">administrator</option>
										'; 
									 break;
			}
			$role .= '</select><br /> <input type="submit" class="sub" value="aktualizuj" /></fieldset></form>';
			
			$act_edit = '<a href="admin,admins,edit,id_'.$row['id'].'.html">Edytuj</a><br />';
			
			$act_access = '<a href="admin,admins,access,id_'.$row['id'].'.html">Uprawnienia</a><br />';
			
			$act_passwd_id = '<a href="admin,admins,passwdid,id_'.$row['id'].'.html">Zmień hasło</a><br />';
			
			if ($row['login'] == $this->_session->getUsername()) {
				$act = '';
				$role = $row['role'];
				$delu = '';
			}	
			else {
				$act = '<a href="admin,admins,'.$sact.',id_'.$row['id'].'.html">'.$act.'</a><br />';
				$delu = '<br /><a href="admin,admins,delete,id_'.$row['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\'")>usuń</a>';
			}	
			//oryginal
			//if ($row['role'] == 'root' && $this->_session->getRole() == 'administrator') 
			//	$role = $row['role'];
			
			//
			if (!($this->_session->getRole() == 'root') )
				$role = $row['role'];
				
			$class = getTableClass();		
			echo '<tr'.$class.'>
					<td>'.$row['id'].$delu.'</td>
					<td>'.$row['login'].'</td>
					<td><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></td>
					<td>'.$role.'</td>
					<td>'.$status.'</td>
					<td>'.$mail.'</td>
					<td>'.$act.$act_edit.$act_passwd_id.$act_access.'</td>
				</tr>';
		}

	echo '</tbody></table>';			
?>		
