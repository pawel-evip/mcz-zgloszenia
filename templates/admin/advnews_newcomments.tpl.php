<?php	
		
echo '<h1>Komentarze oczekujące na akceptację</h1>
<table id="tab-zlecenie">
			<thead>
				<tr>
					<td>ID</td>
					<td>News ID</td>
					<td>Data dodania</td> 
					<td>Autor</td>
					<td>Treść</td>			
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
	
	$rows = (array)$this->rows;
	
	if (count($rows) < 1) echo '<tr><td colspan="6">Nieodnaleziono żadnych nowych komentarzy w bazie.</td></tr>';
	else {	
		foreach($rows as $r) 
		{
			$class = getTableClass();
				
			echo '	<tr'.$class.'>
					<td>'.$r['id'].'<br /><a href="admin,advnews,deletecomment,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
					<td>'.$r['item'].'</td>
					<td>'.date('d.m.Y H:i', $r['add_date']).'</td>
					<td>'.$r['author'].'</td>
					<td>'.$r['contents'].'</td>	
					<td><a href="admin,advnews,activecomment,id_'.$r['id'].'.html">Akceptuj</a> | <a href="admin,advnews,editcomment,id_'.$r['id'].'.html">Edytuj</a></td>
				</tr>';
		}		
	}

	echo '</tbody><tfoot><tr><td colspan="6"></td></tr></tfoot></table>';
	
?>
