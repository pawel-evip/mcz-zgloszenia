<?php	
		
echo '<h1>Realizacje</h1>
<table id="tab-zlecenie">
			<thead>
				<tr>
					<td>ID</td>
					<td>Data dodania</td> 
					<td>Nazwa</td>
					<td>Link</td>			
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
	
	$rows = (array)$this->rows;
	
	if (count($rows) < 1) echo '<tr><td colspan="5">Nie odnaleziono żadnych realizacji w bazie.</td></tr>';
	else {	
		foreach($rows as $r) 
		{
			$class = getTableClass();
				
			echo '	<tr'.$class.'>
					<td>'.$r['id'].'<br /><a href="admin,realizacje,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
					<td>'.date('d.m.Y H:i', $r['add_date']).'</td>
					<td>'.$r['name'].'</td>
					<td><a href="http://'.$r['href'].'" target="_blank">'.$r['href'].'</a></td>	
					<td><a href="admin,realizacje,edit,id_'.$r['id'].'.html">Edytuj</a></td>
				</tr>';
		}		
	}

	echo '</tbody><tfoot><tr><td colspan="5"></td></tr></tfoot></table>';
	
?>
