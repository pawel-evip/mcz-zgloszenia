<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_DbMysql
{
	private static $_instance = null;
	private $_status = false; 
	private $_connection = null;
		
	private function __clone() {}
	private function __construct($dbSettings)
	{
		$this->_status = true;
		$connection = mysql_connect($dbSettings['host'], $dbSettings['user'], $dbSettings['pass']) or die('nie mozna polaczyc sie z db');
		mysql_select_db($dbSettings['name'], $connection) or die('nie mozna wybrac bazy');
		$this->_connection = $connection;
		mysql_query("SET NAMES 'utf8'");
	}
	
	public function __destruct()
	{
	    $this->_status = false; 
	    $this->_connection = '';
	}
		
	public function getStatus()
	{
		return $this->_status;
	}
		
	public static function getInstance($dbSettings)
	{		
		if (is_null(self::$_instance)) self::$_instance = new self($dbSettings);
		return self::$_instance;		
	}		
}

?>
