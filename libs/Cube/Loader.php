<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_Loader
{
    public static function loadClass($class)
    {
        if (class_exists($class, false) || interface_exists($class, false)) {
            return;
        }

        if (null === $dirs) {
            $dirs = array();
        }
        if (is_string($dirs)) {
            $dirs = (array) $dirs;
        }

        $path = str_replace('_', DIRECTORY_SEPARATOR, $class);
        if ($path != $class) {
            $dirPath = dirname($path);
            if (0 == count($dirs)) {
                $dirs = array($dirPath);
            } else {
                foreach ($dirs as $key => $dir) {
                    $dir = rtrim($dir, '\\/');
                    $dirs[$key] = $dir . DIRECTORY_SEPARATOR . $dirPath;
                }
            }
            $file = basename($path) . '.php';
        } else {
            $file = $class . '.php';
        }

        self::loadFile($file, $dirs, true);

        if (!class_exists($class, false) && !interface_exists($class, false)) {
            require_once 'Cube/Exception.php';
            throw new Cube_Exception("Plik \"$file\" istnieje ale klasa \"$class\" nie");
        }
    }


    public static function loadFile($filename, $dirs = null, $once = false)
    {
        if (preg_match('/[^a-z0-9\-_.]/i', $filename)) {
            require_once 'Cube/Exception.php';
            throw new Cube_Exception('Cube_Loader::loadFile: niedozwolone znaki w $filename');
        }
        if (empty($dirs)) {
            $dirs = array();
        } elseif (is_string($dirs))  {
            $dirs = explode(PATH_SEPARATOR, $dirs);
        }
        foreach ($dirs as $dir) {
            $filespec = rtrim($dir, '\\/') . DIRECTORY_SEPARATOR . $filename;
            if (self::isReadable($filespec)) {
                return self::_includeFile($filespec, $once);
            }
        }
        if (self::isReadable($filename)) {
            return self::_includeFile($filename, $once);
        }

        require_once 'Cube/Exception.php';
        throw new Cube_Exception("Nieodnaleziono pliku \"$filename\"");
    }

    protected static function _includeFile($filespec, $once = false)
    {
        if ($once) {
            return include_once $filespec;
        } else {
            return include $filespec ;
        }
    }

    public static function isReadable($filename)
    {
        if (is_readable($filename)) {
            return true;
        }

        $path = get_include_path();
        $dirs = explode(PATH_SEPARATOR, $path);

        foreach ($dirs as $dir) 
		{
            if ('.' == $dir) {
                continue;
            }

            if (is_readable($dir . DIRECTORY_SEPARATOR . $filename)) {
                return true;
            }
        }
        return false;
    }
    
    public static function autoload($class)
    {
        try {
            self::loadClass($class);
            return $class;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function registerAutoload($class = 'Cube_Loader')
    {
        if (!function_exists('spl_autoload_register')) {
            require_once 'Cube/Exception.php';
            throw new Cube_Exception('spl_autoload nie jest zainstalowane');
        }

        self::loadClass($class);
        $methods = get_class_methods($class);
        if (!in_array('autoload', (array) $methods)) {
            require_once 'Cube/Exception.php';
            throw new Cube_Exception("Klasa \"$class\" nie posiada zaimplementowanej metody autoload()");
        }
        spl_autoload_register(array($class, 'autoload'));
    }    
}
