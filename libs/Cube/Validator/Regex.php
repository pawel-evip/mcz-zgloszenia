<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */
require_once 'Cube/Validator/Interface.php';

class Cube_Validator_Regex implements Cube_Validator_Interface
{
	private $_regex;
    
	public function __construct($params) 
	{
		if (isset($params['regex'])) {
			$this->_allowedTags = $params['regex'];
	}
	
    public function validate($value)
    {
		if (!preg_match($this->_regex, $value)) return false;
		return true;
    }
}
